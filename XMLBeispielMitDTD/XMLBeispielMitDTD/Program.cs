﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;        // for XmlTextReader and XmlValidatingReader
using System.Xml.Schema; // for XmlSchemaCollection (which is used later)

namespace XMLBeispielMitDTD
{
    //http://support.microsoft.com/kb/307379/de
    class Program
    {
        private static bool isValid = true;

        static void Main(string[] args)
        {
            XmlTextReader r = new XmlTextReader("../../daten/sample.xml");
            XmlValidatingReader v = new XmlValidatingReader(r);

            v.ValidationType = ValidationType.DTD;
            v.ValidationEventHandler +=
                 new ValidationEventHandler(MyValidationEventHandler);
            
            while (v.Read())
            {
                Console.WriteLine(v.LineNumber+":"+v.LinePosition+"\t"+v.NodeType + "\t" );
            }
            v.Close();

            // Check whether the document is valid or invalid.
            if (isValid)
                Console.WriteLine("Document is valid");
            else
                Console.WriteLine("Document is invalid");

        }

        private static void MyValidationEventHandler(object sender, ValidationEventArgs e)
        {
            isValid = false;
            Console.WriteLine("Validation event\n" + e.Message);
        }
    }
}
